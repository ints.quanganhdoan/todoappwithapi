import "./App.css"

import { useRef, useReducer, useEffect } from "react"
import { FaPlus, FaRegTrashAlt, FaPen } from 'react-icons/fa';

import axios from 'axios'

// initState
const initState = {
  job: '',
  jobs: [],
  count: 0
}

// define Actions
const SET_JOB = 'set_job';
const SET_JOBS = 'set_jobs';

const setJobs = payload => {
  return {
    type: SET_JOBS,
    payload
  }
}

const setJob = payload => {
  return {
    type: SET_JOB,
    payload
  }
}


// create reducer
const reducer = (state, action) => {
  let newState;

  switch (action.type) {
    case SET_JOBS:
      newState = { ...state, jobs: action.payload, count: action.payload.length }
      break;
    case SET_JOB:
      newState = { ...state, job: action.payload }
      break;
    default:
      throw new Error('Invalid actions');
  }

  return newState;
}

function App() {

  const [state, dispatch] = useReducer(reducer, initState);

  const { job, jobs, count } = state;

  const inputRef = useRef();

  const callAPI = () => {
    axios.get('http://192.168.68.86:8080/list')
      .then((response) => {
        dispatch(setJobs(response.data));
      })
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    callAPI();
  }, [])

  const handleSubmit = () => {
    if (job.trim() == '') {
      alert('Bạn chưa nhập công việc vào đây, mời bạn nhập công việc vào');
    }
    else {
      axios.post('http://192.168.68.86:8080/add', { name: job, description: 'Mặc định', status: false })
        .then(() => {
          callAPI();
          dispatch(setJob(''));
          inputRef.current.focus();
        })
        .catch((err) => console.log(err));
    }
  }

  const handleEdit = (jobObj) => {
    const newJob = prompt("Nhập công việc mới:", jobObj.name);
    if (newJob !== null && newJob.trim() !== '') {
      axios.put(`http://192.168.68.86:8080/update/${jobObj._id}`, { name: newJob, description: 'Mặc định', status: false })
        .then(() => {
          callAPI();
        })
        .catch((err) => console.log(err));
    }
  }

  const handleDelete = (id) => {
    axios.delete(`http://192.168.68.86:8080/delete/${id}`)
      .then(() => {
        callAPI();
      })
      .catch((err) => console.log(err));
  }


  return (
    <>
      <div className="todo__content">
        <h3>Todo: {count}</h3>
        <div className="todo_input">
          <input
            type="text"
            placeholder="Enter Todo..."
            value={job}
            ref={inputRef}
            onChange={(e) => dispatch(setJob(e.target.value))}
            className="todo__input_text"
          />
          <button onClick={handleSubmit} className="todo__input_btn"><FaPlus /></button>
        </div>
        <ul className="todo__list">
          {jobs.map((jobObj) => {
            return (
              <li className="todo__list_data" key={jobObj._id}>
                {jobObj.name}
                <div className="todo__list_data_btn">
                  <button
                    onClick={() => handleEdit(jobObj)}
                    className="todo__list_btn_edit"
                  >
                    <FaPen />
                  </button>
                  <button
                    onClick={() => handleDelete(jobObj._id)}
                    className="todo__list_btn_delete"
                  >
                    <FaRegTrashAlt />
                  </button>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    </>
  )
}

export default App
